/**
* @param {number[][]} points
* @return {number}
*/
var findSolution = function (points) {
    var solution = 0;
    if (points.length === 1) solution = 1;
    for (var i = 0; i < points.length; i++) {
      var a = points[i];
      var overlap = 0;
      var vertical = 0;
      var curMax = 0;
      var dic = {};
  
      for (var j = i + 1; j < points.length; j++) {
        var b = points[j];
  
        if (a === b) {
          overlap++;
        } else if (a[0] === b[0]) {
          vertical++;
        } else {
          var m = ((a[1] - b[1]) / (a[0] - b[0])).toFixed(10);
          m = m.toString();
  
          if (!dic[m]) {
            dic[m] = 2;
          } else {
            dic[m]++;
          }
  
          curMax = Math.max(curMax, dic[m]);
        }
  
      }
      solution = Math.max(solution, curMax + overlap, vertical + overlap)
    }
  
    return solution;
  }
  
  module.exports = {
    findSolution
  };
  