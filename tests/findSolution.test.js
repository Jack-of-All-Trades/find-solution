var b = require('../findSolution');
var input = require('../input.json');

input.forEach(data => {
  var solution = b.findSolution(data);
  console.log(solution);
});
